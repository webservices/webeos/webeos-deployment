{{- define "webeos.imageStreamName" -}}
{{- printf "%s:%s" .Release.Name .Values.imageStream.tag | trunc 63 | trimSuffix "-" -}}
{{- end -}}
