apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: {{ .Release.Name }}
rules:
- apiGroups:
  - webeos.webservices.cern.ch
  resources:
  - 'webeossites'
  verbs:
  - get
  - list
  - watch
# Allow webeos pods to run as anyuid (so httpd can start as root)
# and access to hostPath (for EOS mount points).
- apiGroups:
  - security.openshift.io
  resourceNames:
  - anyuid
  resources:
  - securitycontextconstraints
  verbs:
  - use

---
# This is necessary so we can fetch on webeos-config-operator the real Pod Network
# insead of hardcoded IP addresses, see: https://gitlab.cern.ch/webservices/webeos/webeos-config-operator/-/issues/58
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: webeos-view-clusternetworks-{{ .Release.Name }}
  annotations:
    description: Read permissions on networks since webeos-config-operator needs access to Pod Network
rules:
- apiGroups:
  - config.openshift.io
  resources:
  - 'networks'
  verbs:
  - get
  - list
  - watch

---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: {{ .Release.Name }}
subjects:
- kind: ServiceAccount
  name: {{ .Release.Name }}
roleRef:
  kind: Role
  name: {{ .Release.Name }}
  apiGroup: rbac.authorization.k8s.io

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: webeos-view-clusternetworks-{{ .Release.Name }}
subjects:
- kind: ServiceAccount
  name: {{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
roleRef:
  kind: ClusterRole
  name: webeos-view-clusternetworks-{{ .Release.Name }}
  apiGroup: rbac.authorization.k8s.io

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    webeos.cern.ch/place-new-webeos-sites: {{ .Values.placeNewWebeosSites | quote }}
  annotations:
    webeos.cern.ch/webeos-site-path-regex: {{ .Values.webeosSitePathRegex | quote }}
    description: The serviceAccount to be used by WebEOS deployment
# With this flag we are disabling the auto mount of SA token in the containers to
# avoid that users might access this token through the httpd container.
automountServiceAccountToken: false
